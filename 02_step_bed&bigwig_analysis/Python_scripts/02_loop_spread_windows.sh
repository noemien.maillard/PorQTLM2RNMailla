#!/bin/bash
# Creation date: 2023/03/30
# Last review: 2023/07/04
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: QTL1_porc (M2R)
# Script aim: loop 02_spread_windows.py script on all available tsv files before comparing biodata and predictions.

# common directory to target files
dir=
# input and output biological data directories
input_bio_dir="tables/bio"
output_bio_dir="post_transformation/bio"
# input and output predictions data directories
input_pred_dir="tables/pred"
output_pred_dir="post_transformation/pred"
for mark in `ls $dir`
do
    for tissue in `ls $dir/$mark`
    do
        for biosampl in `ls $dir/$mark/$tissue/$input_bio_dir`
        do
            # run script on biological data
            output_file=${biosampl%.*}  # remove extension from input file
            python3 "02_spread_ranges.py" $dir/$mark/$tissue/$input_bio_dir/$biosampl $dir/$mark/$tissue/$output_bio_dir/$output_file"_spread.tsv"
        done
        for predsampl in `ls $dir/$mark/$tissue/$input_pred_dir`
        do
            # run script on predictions data
            output_file=${predsampl%.*}  # remove extension from input file
            python3 "02_spread_ranges.py" $dir/$mark/$tissue/$input_pred_dir/$predsampl $dir/$mark/$tissue/$output_pred_dir/$output_file"_spread.tsv"
        done
    done
done
