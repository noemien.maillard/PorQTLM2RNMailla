#!/bin/bash
# Creation date: 2023/03/08
# Last review: 2023/07/04
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: QTL1_porc (M2R)
# Script aim: loop 01_range_bed.py script on all available .bed files.
#
# set input and output directories
input="/mnt/c/Users/nomaillard/Documents/02_ucdavis/bed/"
output="/mnt/c/Users/nomaillard/Documents/02_ucdavis/check_script/"
for file in `ls $input`;
do
	output_file=${file%.*}  # remove extension from input file
	python3.10 "01_range_bed.py" --input $input$file --output $output$output_file"_ranged.bed" --chr 1 --min 270300000 --max 270800000
done
