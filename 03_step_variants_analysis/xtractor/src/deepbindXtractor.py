# Creation date: 2023/04/26
# Last review: 2023/07/05
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: QTL1_porc (M2R)
# Script aim: Extract specified columns from deepbind predictions output.

import logging
import pandas as pd


def reorder_cols(df: pd.DataFrame, firsts_cols: list) -> pd.DataFrame:
    """Reorder columns with first columns first.

    Args:
        df (pd.DataFrame): Input dataframe to reorder.
        firsts_cols (list): Columns to keep first.

    Returns:
        pd.DataFrame: dataframe with reordered columns.
    """
    # take first columns
    new_df = df[firsts_cols]
    # remove columns in first_columns
    df.drop(columns=firsts_cols, inplace=True)
    # concat first columns dataframe with other columns
    return pd.concat([new_df, df], axis=1)


def drop_pos(df: pd.DataFrame, filter_pos: list) -> pd.DataFrame:
    """Drop columns with positions to filter.

    Args:
        df (pd.DataFrame): Input dataframe.
        filter_pos (list): Positions to drop.

    Returns:
        pd.DataFrame: Dataframe with droped columns.
    """
    # drop positions (with ~ symbol) in POS column or write error in log file
    try:
        return df[~df['POS'].isin(filter_pos)]
    except KeyError:
        logging.warn('Missed specified positions by using "POS" colname.')

    # fill log file and return df without droped positions
    logging.warn('All positions extracted.')
    return df


def keep_pos(df: pd.DataFrame, filter_pos: list) -> pd.DataFrame:
    """Keep only specified positions.

    Args:
        df (pd.DataFrame): Input dataframe.
        filter_pos (list): Positions to keep.

    Returns:
        pd.DataFrame: Dataframe with kept columns.
    """
    # keep position in POS column or write error in log file
    try:
        return df[df['POS'].isin(filter_pos)]
    except KeyError:
        logging.warn('Missed specified positions by using "POS" colname.')

    # fill log file and return df with specified positions only
    logging.warn('All positions extracted.')
    return df


def find_patterns_or_logic(line: pd.DataFrame, cond_dict: dict) -> bool:
    """Return True if pattern is observed in given column, else False.
    At least 1 pattern has to be found among all conditions (cond_dict).

    Args:
        line (pd.DataFrame): Line from database associated with given ID from deepbind dataframe.
        cond_dict (dict): Dictionary containing all patterns for each column/condition.

    Returns:
        bool: True if pattern found else False.
    """
    # for each column (from database)
    for col in line.columns:
        if col not in cond_dict.keys(): continue
        # if column must be checked (in cond_dict keys)
        # if column is experiment details return True. Patterns like 'HEK' are tolerated
        if col == "Experiment Details":
            for pattern in cond_dict[col]:
                if pattern in line[col].to_string():
                    logging.info(pattern)
                    return True
        # if column is not experiment details return True. Patterns must be exactly as in table
        if line[col].isin(cond_dict[col]).any():
            logging.info(' '.join(line[col].to_string().split(' ')[4:]))
            return True
    # if pattern not in cond dict, return False
    return False


def find_patterns_and_logic(line: pd.DataFrame, cond_dict: dict) -> bool:
    """Return True if pattern is observed in given column, else False.
    All patterns (1 per column in database) has to be found among all conditions (cond_dict).

    Args:
        line (pd.DataFrame): Line from database associated with given ID from deepbind dataframe.
        cond_dict (dict): Dictionary containing all patterns for each column/conditions.

    Returns:
        bool: True if pattern found else False.
    """
    # dict to store all patterns found for each columns (in database)
    patterns_dict = {}
    # for each column (from database)
    for col in line.columns:
        if col not in cond_dict.keys(): continue
        # if column must be checked (in cond_dict keys)
        # if columns is experiment details, check presence (or absence) of pattern and store it in patterns_dict
        # patterns like 'HEK' are tolerated
        if col == "Experiment Details":
            for pattern in cond_dict[col]:
                if pattern in line[col].to_string():
                    patterns_dict[col] = pattern
            continue
        # if column is not experiment details, check presence (or absence) of pattern and store it in patterns_dict
        if line[col].isin(cond_dict[col]).any():
            patterns_dict[col] = ' '.join(line[col].to_string().split(' ')[4:])

    # if as much patterns found than pattern checked, return True else False
    if len(patterns_dict) == len(cond_dict):
        logging.info(patterns_dict)
        return True
    logging.info(patterns_dict)
    return False


def drop_cols(df: pd.DataFrame, drop_dict: dict, db: pd.DataFrame, logic: str, filter_pos: list, drop_positions: bool) -> pd.DataFrame:
    """Drop columns given logic.

    Args:
        df (pd.DataFrame): Input dataframe.
        drop_dict (dict): Conditions to drop.
        db (pd.DataFrame): Database.
        logic (str): AND means all conditions/columns have to be true, OR means at least 1.
        filter_pos (list): Positions to keep or drop given drop_position argument (optional).
        drop_positions (bool): If True, drop positions else keep positions.

    Returns:
        pd.DataFrame: Dataframe containing cols drop and rows droped or kept.
    """
    new_df = pd.DataFrame()
    # drop columns if all conditions are checked
    if logic == 'AND':
        # for each column
        for name in df.columns:
            # if no ID in database (like info columns), concat dataframe with new_df
            if db.loc[db['ID'] == name].empty:
                new_df = pd.concat([new_df, df[name]], axis=1)
            # drop column in place if all patterns found
            if find_patterns_and_logic(line=db.loc[db['ID'] == name], cond_dict=drop_dict):
                logging.info(db.loc[db['ID'] == name])
                df.drop(columns=name, inplace=True)
    # drop columns if at least 1 condition is checked
    elif logic == 'OR':
        # for each column
        for name in df.columns:
            # if no ID in database (i.e. info cols), concat dataframe with new_df
            if db.loc[db['ID'] == name].empty:
                new_df = pd.concat([new_df, df[name]], axis=1)
            # drop column in place if at least 1 pattern is found
            if find_patterns_or_logic(line=db.loc[db['ID'] == name], cond_dict=drop_dict):
                logging.info(db.loc[db['ID'] == name])
                df.drop(columns=name, inplace=True)

    # keep or drop positions if filter_pos is not None (given drop_position value) and return final dataframe
    if filter_pos in None:
        return df
    elif filter_pos is not None and not drop_positions:
        return keep_pos(df=df, filter_pos=filter_pos)
    elif filter_pos is not None and drop_positions:
        return drop_pos(df=df, filter_pos=filter_pos)


def keep_cols(df: pd.DataFrame, keep_dict: dict, db: pd.DataFrame, logic: str, filter_pos: list, drop_positions: bool) -> pd.DataFrame:
    """Keep columns given logic.

    Args:
        df (pd.DataFrame): Input dataframe.
        keep_dict (dict): Conditions to keep.
        db (pd.DataFrame): Database.
        logic (str): AND means all conditions/columns have to be true, OR means at least 1.
        filter_pos (list): Positions to keep or drop given drop_position argument (optional).
        drop_positions (bool): If True, drop positions else keep positions.

    Returns:
        pd.DataFrame: Dataframe containing cols drop and rows droped or kept.
    """
    new_df = pd.DataFrame()
    # keep columns if all conditions are checked
    if logic == 'AND':
        # for each column
        for name in df.columns:
            # if no ID in database (i.e. info cols), concat dataframe with new_df
            if db.loc[db['ID'] == name].empty:
                new_df = pd.concat([new_df, df[name]], axis=1)
            # keep columns by concatenating with new_df if all patterns found
            if find_patterns_and_logic(line=db.loc[db['ID'] == name], cond_dict=keep_dict):
                logging.info(db.loc[db['ID'] == name].to_string().split('\n')[1:])
                new_df = pd.concat([new_df, df[name]], axis=1)
    # keep columns if at least 1 condition is checked
    elif logic == 'OR':
        # for each column
        for name in df.columns:
            # if no ID in database (i.e. info cols), concat dataframe with new_df
            if db.loc[db['ID'] == name].empty:
                new_df = pd.concat([new_df, df[name]], axis=1)
            # keep columns by concatenating with new_df if at least 1 pattern is found
            if find_patterns_or_logic(line=db.loc[db['ID'] == name], cond_dict=keep_dict):
                logging.info(db.loc[db['ID'] == name].to_string().split('\n')[1:])
                new_df = pd.concat([new_df, df[name]], axis=1)

    # keep or drop positions if filter_pos is not None (given drop_position value) and return final dataframe
    if filter_pos is None:
        return new_df
    elif filter_pos is not None and not drop_positions:
        return keep_pos(df=new_df, filter_pos=filter_pos)
    elif filter_pos is not None and drop_positions:
        return drop_pos(df=new_df, filter_pos=filter_pos)


def deepbind(
        input: str,
        output: str,
        sep: str,
        database: str,
        sep_db: str,
        skip: int,
        filter: str,
        filter_file: str,
        logic: str,
        filter_pos_file: str,
        info_cols_file: str,
        info_cols_sep: str,
        drop_positions: bool
        ):
    """Deepbind main management function. Call functions to keep/drop columns/positions.
    Then generate output file if output is not None else print result in terminal.

    Args:
        input (str): Input table from deepbind predictions.
        output (str): Output file name.
        sep (str): Input table separator.
        database (str): Database.
        sep_db (str): Database separator.
        skip (int): Lines to skip in database file (lines before table).
        filter (str): Filter type, keep or drop.
        filter_file (str): File containing conditions to filter.
        logic (str): Logic to filter, AND or OR.
        filter_pos_file (str): Positions to filter.
        info_cols_file (str): file containing info columns.
        info_cols_sep (str): Separator of info_cols_file.
        drop_positions (bool): If True, positions are droped else they are kept.
    """
    # read input table and info columns as pandas Dataframe
    df = pd.read_csv(input, sep=sep, engine='python')
    info_cols = pd.read_csv(info_cols_file, sep=info_cols_sep, engine='python')
    # if df info columns are different than info_cols columns, change for info_cols columns and log it
    # this step suppose df columns are from output predictions (lead to an error if cols names are not chr, pos, ref and alt)
    if df.columns.to_list()[:5] != info_cols.columns.to_list():
        df.drop(columns=['chr', 'pos', 'ref', 'alt'], inplace=True)
        logging.info(f'Input file columns droped to replace by {info_cols.columns.to_list()}.')
        df = pd.concat([info_cols, df], axis=1)

    # store info cols columns (used for reordering)
    list_info_cols = info_cols.columns.to_list()

    # read database
    db = pd.read_csv(database, sep=sep_db, engine='python', skiprows=skip)

    # read filter file and store filters in dictionary
    filter_dict = dict()
    with open(filter_file) as filter_list:
        for line in filter_list:
            line = line.split(',')
            line = [word.strip('\n') for word in line]
            filter_dict[line[0]] = line[1:]

    # store filter pos in a list and turn into int
    if filter_pos_file is not None:
        filter_pos = open(filter_pos_file).read().splitlines()
        filter_pos = [int(pos) for pos in filter_pos]
    else:
        filter_pos = None

    # call drop or keep functions and output in file or terminal
    # generate output file
    if output:
        # drop columns
        if filter == 'drop':
            if logic == 'AND':
                logging.info(f'Run mode: drop columns\nLogic: AND\nDatabase: {database}\nDrop-file: {filter_file}\nInfo cols file: {info_cols_file}\nColumns catched keywords/patterns')
            elif logic == 'OR':
                logging.info(f'Run mode: drop columns\nLogic: OR\nDatabase: {database}\nDrop-file: {filter_file}\nInfo cols file: {info_cols_file}\nColumns catched keywords/patterns')
            reorder_cols(df=drop_cols(df=df, drop_dict=filter_dict, db=db, logic=logic, filter_pos=filter_pos, drop_positions=drop_positions), firsts_cols=list_info_cols).to_csv(path_or_buf=output, header=True, index=False)
        # keep columns
        elif filter == 'keep':
            if logic == 'AND':
                logging.info(f'Run mode: keep columns\nLogic: AND\nDatabase: {database}\nKeep-file: {filter_file}\nInfo cols file: {info_cols_file}\nColumns catched keywords/patterns')
            elif logic == 'OR':
                logging.info(f'Run mode: keep columns\nLogic: OR\nDatabase: {database}\nKeep-file: {filter_file}\nInfo cols file: {info_cols_file}\nColumns catched keywords/patterns')
            reorder_cols(df=keep_cols(df=df, keep_dict=filter_dict, db=db, logic=logic, filter_pos=filter_pos, drop_positions=drop_positions), firsts_cols=list_info_cols).to_csv(path_or_buf=output, header=True, index=False)
    # print in terminal
    else:
        # drop columns
        if filter == 'drop':
            print(reorder_cols(df=drop_cols(df=df, drop_dict=filter_dict, db=db, logic=logic, filter_pos=filter_pos, drop_positions=drop_positions), firsts_cols=list_info_cols))
        # keep columns
        elif filter == 'keep':
            print(reorder_cols(df=keep_cols(df=df, keep_dict=filter_dict, db=db, logic=logic, filter_pos=filter_pos, drop_positions=drop_positions), firsts_cols=list_info_cols))
