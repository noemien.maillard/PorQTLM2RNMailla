# Creation date: 2023/04/26
# Last review: 2023/07/04
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: QTL1_porc (M2R)
# Script aim: Main xtractor file, take options and run script.

import logging
import sys


def get_options():
    """Set up and parse arguments.

    Returns:
        Namespace: Parser with arguments.
    """
    import argparse

    parser = argparse.ArgumentParser()
    
    posgroup = parser.add_argument_group('Positional Options')
    posgroup.add_argument(
        'mode',
        help='Accepted options: "extract" to extract data and generate new csv file and  \
                "bestscores" to calcul mean given a specific condition (all SNP, SNP tissue specific,  \
                SNP experiments specific).'
    )

    commongroup = parser.add_argument_group('Common Options for extract and mean modes')
    commongroup.add_argument(
        '-i',
        '--input',
        required=True,
        help='Input file. Required option.'
    )
    commongroup.add_argument(
        '-s',
        '--sep',
        default=",",
        help='Values separator of input file. Note: deepbind sep is tab and enformer sep is coma. Default to coma.'
    )
    commongroup.add_argument(
        '-o',
        '--output',
        help='Output file, csv format (with extension).'
    )
    commongroup.add_argument(
        '--log',
        help='Log file path or name. Default to current_script_directory/Xtractor.log.'
    )

    extractgroup = parser.add_argument_group('Common Options for extraction mode')
    extractgroup.add_argument(
        '--network',
        help='The neural network used to generate input file. Must be "deepbind" or "enformer". Required option.'
    )
    extractgroup.add_argument(
        '--filter-pos-file',
        help='File containing positions of interest. If no file is given, all positions are kept. \
            Each line must contain 1 position.'
    )
    extractgroup.add_argument(
        '--drop-positions',
        action='store_true',
        help='If precised, drop rows with given positions (from positions file) instead of keeping them.'
    )
    extractgroup.add_argument(
        '--info-cols-file',
        help='File containing informations columns from vcf file. Required option.'
    )
    extractgroup.add_argument(
        '--info-cols-sep',
        default=';',
        help='Separator used to turn info-col-file into pandas DataFrame. Default to ";", direct excel output.'
    )

    extractdeepgroup = parser.add_argument_group('Deepbind Specific Options')
    extractdeepgroup.add_argument(
        '--database',
        help='Database file containing informations related to IDs conditions.'
    )
    extractdeepgroup.add_argument(
        '--sep-db',
        default='\t',
        help='Database table separator. Default to tab.'
    )
    extractdeepgroup.add_argument(
        '--db-skip',
        default=21,
        type=int,
        help='Number of lines to skip before database table. Default to 21.'
    )
    extractdeepgroup.add_argument(
        '--filter',
        default='keep',
        help='Precise if keep or drop values. Possible values: "keep", "drop".  \
            Default to "keep".'
    )
    extractdeepgroup.add_argument(
        '--filter-file',
        help='Input file to filter values (where values can be pattern(s) for "Experimental Details" column).  \
            Coma separated values, first value is colname, then each values to keep/drop.'
    )
    extractdeepgroup.add_argument(
        '--logic',
        default='AND',
        help='Logical choice between columns. Use "AND" if you want all filters passed or "OR" if you want at least one filter passed.  \
            Default to "AND".'
    )

    extractenfgroup = parser.add_argument_group('Enformer Specific Options')
    extractenfgroup.add_argument(
        '--with-index-col',
        action='store_true',
        help="If precised, means first column of enformer table input is index. Warning: enformer predictions output first column is index."
    )
    extractenfgroup.add_argument(
        '--drop',
        nargs='+',
        help='Columns to drop by name or pattern contained in col names.'
    )
    extractenfgroup.add_argument(
        '--drop-file',
        help='Input file with columns to drop. Each line is a column or pattern to drop.'
    )
    extractenfgroup.add_argument(
        '--keep',
        nargs='+',
        help='Columns to keep by name or pattern contained in col names.'
    )
    extractenfgroup.add_argument(
        '--keep-file',
        help='Input file with columns to keep. Each line is a column or pattern to keep.'
    )

    bestmeansgroup = parser.add_argument_group("Common Options for mean mode")
    bestmeansgroup.add_argument(
        '--best-col-scores',
        action='store_true',
        help='If NOT precised (default), calculates n absolute best_means among all experiments and keep input table format. \
            2 output files will be generated storing n best absolute means (abs(mean(scores))) \
            and n best means of absolute scores (mean(abs(scores)))\n \
            If precised, the n best absolute scores will be kept for each column/experiment.  \
            Output file will contain 3 columns: condition, position and score. \
            The table will be of size conditions*n_best_means.'
    )
    bestmeansgroup.add_argument(
        '-n',
        type=int,
        default=50,
        help='n scores to keep for each experiment (best-col-scores=True) or each mean (best-col-scores=False). \
            Default to 50.'
    )
    return parser.parse_args()

def main():
    args = get_options()
    # generate log file according to log option
    # if log option, add '.log' if not in file name
    if args.log:
        if args.log[-4:] == '.log':
            log_file = args.log
        else:
            log_file = args.log + '.log'
    # if not log option, use input or output file name to set log file name
    elif not args.log and args.output:
        log_file = args.output[:-4] + '.log'
    elif not args.log and not args.output:
        log_file = args.input[:-4] + '_logfrominputname.log'
    
    # extract deepbind
    if getattr(args, 'mode') == 'extract' and args.network == 'deepbind':
        from .deepbindXtractor import deepbind
        
        logging.basicConfig(
            filename=log_file,
            filemode='w',
            encoding='utf-8',
            level=logging.DEBUG,
            format='%(asctime)s %(message)s',
            datefmt='%Y/%d/%m %I:%M:%S %p'
            )
        logging.info(f'Input file: {args.input}')
        logging.info(f'Output file: {args.output}')
        logging.info(f'Logging file: {log_file}')
        logging.info('Xtractor: deepbind')

        deepbind(
            input=args.input,
            output=args.output,
            sep=args.sep,
            database=args.database,
            sep_db=args.sep_db,
            skip=args.db_skip,
            filter=args.filter,
            filter_file=args.filter_file,
            logic=args.logic,
            info_cols_file=args.info_cols_file,
            info_cols_sep=args.info_cols_sep,
            filter_pos_file=args.filter_pos_file,
            drop_positions=args.drop_positions
            )
    # extract enformer
    elif getattr(args, 'mode') == 'extract' and args.network == 'enformer':
        from .enformerXtractor import enformer

        logging.basicConfig(
            filename=log_file,
            filemode='w',
            encoding='utf-8',
            level=logging.DEBUG,
            format='%(asctime)s %(message)s',
            datefmt='%Y/%d/%m %I:%M:%S %p'
            )
        logging.info(f'Input file: {args.input}')
        logging.info(f'Output file: {args.output}')
        logging.info(f'Logging file: {log_file}')
        logging.info('Xtractor: enformer')

        # exit if drop and keep are used together
        if (args.drop is not None or args.drop_file is not None) and (args.keep is not None or args.keep_file is not None):
            logging.error("Run aborted. Drop and keep options used together.\nExit error code: 1")
            exit("You cannot use a drop/drop-file and keep/keep-file option together.")

        enformer(
            input=args.input,
            output=args.output,
            sep=args.sep,
            index_col=args.with_index_col,
            info_cols_file=args.info_cols_file,
            info_cols_sep=args.info_cols_sep,
            drop=args.drop,
            drop_file=args.drop_file,
            keep=args.keep,
            keep_file=args.keep_file,
            filter_pos_file=args.filter_pos_file,
            drop_positions=args.drop_positions
            )
    # calculate best means
    elif getattr(args, 'mode') == 'bestscores' and not args.best_col_scores:
        from .meanXtractor import best_means
        
        logging.basicConfig(
            filename=log_file,
            filemode='w',
            encoding='utf-8',
            level=logging.DEBUG,
            format='%(asctime)s %(message)s',
            datefmt='%Y/%d/%m %I:%M:%S %p'
            )
        logging.info(f'Input file: {args.input}')
        logging.info(f'Output file: {args.output}')
        logging.info(f'Logging file: {log_file}')
        logging.info(f'Mode: mean')
        logging.info(f'Keep {args.n} best absolute means.')
        output1 = args.output[:-4] + 'BestAbsoluteSnpMeans' + args.output[-4:]
        output2 = args.output[:-4] + 'BestSnpAbsoluteMeans' + args.output[-4:]
        logging.info(f'Output files:\n{output1}\n{output2}')

        best_means(
            input=args.input,
            output=args.output,
            sep=args.sep,
            n_scores=args.n
        )
    elif getattr(args, 'mode') == 'bestscores' and args.best_col_scores:
        from .meanXtractor import best_col_scores

        logging.basicConfig(
            filename=log_file,
            filemode='w',
            encoding='utf-8',
            level=logging.DEBUG,
            format='%(asctime)s %(message)s',
            datefmt='%Y/%d/%m %I:%M:%S %p'
            )
        logging.info(f'Input file: {args.input}')
        logging.info(f'Output file: {args.output}')
        logging.info(f'Logging file: {log_file}')
        logging.info(f'Mode: best_col_scores (best score for each experiment)')
        logging.info(f'Keep {args.n} best scores for each experiment.')
        logging.info(f'Output file: {args.output}')

        best_col_scores(
            input=args.input,
            output=args.output,
            sep=args.sep,
            n_scores=args.n
        )
    else:
        logging.error('Positional argument mode must be "extract" or "bestmeans". If you want to extract data, network must be "deepbind" or "enformer".\nExit code: 1')
        sys.exit('Positional argument mode must be "extract" or "bestmeans". If you want to extract data, network must be "deepbind" or "enformer".')


if __name__ == '__main__':
    main()