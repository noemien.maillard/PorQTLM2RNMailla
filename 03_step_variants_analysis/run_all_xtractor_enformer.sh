#! /bin/bash

xtractor=/mnt/c/nomaillard/projet/PorQTLM2RNMaillard/03_step_variants_analysis/xtractor/xtractor.py

# common files for enformer and enformer extraction
positions_to_keep='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/positions_to_keep.txt'
info_cols='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/info_cols.csv'

# inputs
enformer_first_filter='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_enformer/params_files/enformer_drop_cols.txt'
enformer_second_filter='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_enformer/params_files/enformer_keep_cols.txt'
enformer_keep_liver='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_enformer/params_files/enformer_keep_liver.txt'
# outputs
enformer_input='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/20240404_VariantsPhases_ok.enformer.txt'
enformer_output1='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_enformer/step1_enformer_dropcols.csv'
enformer_output2='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_enformer/step2_enformer_keepcols.csv'
enformer_output3_DNASE='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_enformer/keep_exp_DNASE.csv'
enformer_output3_ChIP='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_enformer/keep_exp_ChIP.csv'
enformer_output3_ATAC='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_enformer/keep_exp_ATAC.csv'
enformer_output3_CAGE='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_enformer/keep_exp_CAGE.csv'
enformer_output4_muscle='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_enformer/keep_tissue_muscle.csv'
enformer_output4_adipose='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_enformer/keep_tissue_adipose.csv'
enformer_output4_liver='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_enformer/keep_tissue_liver.csv'
enformer_best_cols='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_enformer/best_col_means.csv'
enformer_bestscores1='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_enformer/bestscores_onstep2.csv'
enformer_bestscores2_DNASE='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_enformer/bestscores_exp_DNASE.csv'
enformer_bestscores2_ChIP='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_enformer/bestscores_exp_ChIP.csv'
enformer_bestscores2_ATAC='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_enformer/bestscores_exp_ATAC.csv'
enformer_bestscores2_CAGE='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_enformer/bestscores_exp_CAGE.csv'
enformer_bestscores3_muscle='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_enformer/bestscores_tissue_muscle.csv'
enformer_bestscores3_adipose='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_enformer/bestscores_tissue_adipose.csv'
enformer_bestscores3_liver='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_enformer/bestscores_tissue_liver.csv'

### run extractions
# drop cols
echo 'start enformer 1st extraction drop columns...'
$xtractor extract \
    --input $enformer_input \
    --output $enformer_output1 \
    --network 'enformer' \
    --filter-pos-file $positions_to_keep \
    --info-cols-file $info_cols \
    --with-index-col \
    --drop-file $enformer_first_filter
echo 'enformer first extraction done.'

# keep cols of interest
echo 'start second extraction keep columns...'
$xtractor extract \
    --input $enformer_output1 \
    --output $enformer_output2 \
    --network 'enformer' \
    --info-cols-file $info_cols \
    --keep-file $enformer_second_filter
echo 'enformer second extraction done.'

# filter on experiment DNASE
echo 'start extraction on experiment DNASE...'
$xtractor extract \
    --input $enformer_output2 \
    --output $enformer_output3_DNASE \
    --network 'enformer' \
    --info-cols-file $info_cols \
    --keep 'DNASE'
echo 'enformer extraction on experiment DNASE done.'

# filter on experiment ChIP
echo 'start extraction on experiment ChIPseq...'
$xtractor extract \
    --input $enformer_output2 \
    --output $enformer_output3_ChIP \
    --network 'enformer' \
    --info-cols-file $info_cols \
    --keep 'CHIP'
echo 'enformer extraction on experiment ChIPseq done.'

# filter on experiment ATAC
echo 'start extraction on experiment ATACseq...'
$xtractor extract \
    --input $enformer_output2 \
    --output $enformer_output3_ATAC \
    --network 'enformer' \
    --info-cols-file $info_cols \
    --keep 'ATAC'
echo 'enformer extraction on experiment ATACseq done.'

# filter on experiment CAGE
echo 'start extraction on experiment CAGE...'
$xtractor extract \
    --input $enformer_output2 \
    --output $enformer_output3_CAGE \
    --network 'enformer' \
    --info-cols-file $info_cols \
    --keep 'CAGE'
echo 'enformer extraction on experiment CAGE done.'

# filter on tissue muscle
echo 'start extraction on tissue muscle...'
$xtractor extract \
    --input $enformer_output2 \
    --output $enformer_output4_muscle \
    --network 'enformer' \
    --info-cols-file $info_cols \
    --keep 'muscl' 'Muscl' 'myo' 'Myo' 'A673' 'smooth' 'Smooth' 'squel' 'Squel' 'LHCN' 'SJCRH30' 'psoas'
echo 'enformer extraction on tissue muscle done.'

# filter on tissue adipose
echo 'start extraction on tissue adipose...'
$xtractor extract \
    --input $enformer_output2 \
    --output $enformer_output4_adipose \
    --network 'enformer' \
    --info-cols-file $info_cols \
    --keep 'adip' 'Adip' 'lipo' 'Lipo' 'omental' 'Omental' 'fat' 'Fat'
echo 'enformer extraction on tissue adipose done.'

# filter on tissue liver
echo 'start extraction on tissue liver...'
$xtractor extract \
    --input $enformer_output2 \
    --output $enformer_output4_liver \
    --network 'enformer' \
    --info-cols-file $info_cols \
    --keep-file $enformer_keep_liver
echo 'enformer extraction on tissue liver done.'

### run best means
# best col means
echo 'start calc best means on cols of step2 dataset...'
$xtractor bestscores \
    --input $enformer_output2 \
    --output $enformer_best_cols \
    --best-col-scores
echo 'enformer best means on cols of step2 done.'

# means on step2 dataset
echo 'start calc 50 best means on step2 dataset...'
$xtractor bestscores \
    --input $enformer_output2 \
    --output $enformer_bestscores1
echo 'enformer 50 best means on step2 dataset done.'

# means on DNASE filtered dataset
echo 'start calc 50 best means on DNASE filtered dataset...'
$xtractor bestscores \
    --input $enformer_output3_DNASE \
    --output $enformer_bestscores2_DNASE
echo 'enformer 50 best means on DNASE filtered dataset done.'

# means on ChIPseq filtered dataset
echo 'start calc 50 best means on ChIPseq filtered dataset...'
$xtractor bestscores \
    --input $enformer_output3_ChIP \
    --output $enformer_bestscores2_ChIP
echo 'enformer 50 best means on ChIPseq filtered dataset done.'

# means on ATACseq filtered dataset
echo 'start calc 50 best means on ATACseq filtered dataset...'
$xtractor bestscores \
    --input $enformer_output3_ATAC \
    --output $enformer_bestscores2_ATAC
echo 'enformer 50 best means on ATAC filtered dataset done.'

# means on CAGE filtered dataset
echo 'start calc 50 best means on CAGE filtered dataset...'
$xtractor bestscores \
    --input $enformer_output3_CAGE \
    --output $enformer_bestscores2_CAGE
echo 'enformer 50 best means on CAGE filtered dataset done.'

# means on muscle filtered dataset
echo 'start calc 50 best means on muscle filtered dataset...'
$xtractor bestscores \
    --input $enformer_output4_muscle \
    --output $enformer_bestscores3_muscle
echo 'enformer 50 best means on muscle filtered dataset done.'

# means on adipose filtered dataset
echo 'start calc 50 best means on adipose filtered dataset...'
$xtractor bestscores \
    --input $enformer_output4_adipose \
    --output $enformer_bestscores3_adipose
echo 'enformer 50 best means on adipose filtered dataset done.'

# means on liver filtered dataset
echo 'start calc 50 best means on liver filtered dataset...'
$xtractor bestscores \
    --input $enformer_output4_liver \
    --output $enformer_bestscores3_liver
echo 'enformer 50 best means on liver filtered dataset done.'
