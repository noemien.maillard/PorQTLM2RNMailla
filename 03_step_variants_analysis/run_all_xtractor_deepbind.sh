#! /bin/bash

xtractor=/mnt/c/nomaillard/projet/PorQTLM2RNMaillard/03_step_variants_analysis/xtractor/xtractor.py

# common files for deepbind and enformer extraction
positions_to_keep='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/positions_to_keep.txt'
info_cols='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/info_cols.csv'

# deepbind database
database='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/db.tsv'
# inputs
deepbind_first_filter='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_deepbind/params_files/deepbind_keep_logicAND_sp_exp.txt'
deepbind_second_filter='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_deepbind/params_files/deepbind_keep_logicOR_exp_hepg2.txt'
deepbind_filter_PBM='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_deepbind/params_files/deepbind_keep_expPBM.txt'
deepbind_filter_ChIP='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_deepbind/params_files/deepbind_keep_expChIP.txt'
deepbind_filter_RNA='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_deepbind/params_files/deepbind_keep_expRNA.txt'
# outputs
deepbind_input='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/20240404_VariantsPhases_ok.diff.deepbind.csv'
deepbind_output1='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_deepbind/step1_keep_sp_exp.csv'
deepbind_output2='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_deepbind/step2_keep_sp_exp_hepg2.csv'
deepbind_output3_1='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_deepbind/keep_exp_PBM.csv'
deepbind_output3_2='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_deepbind/keep_exp_ChIP.csv'
deepbind_output3_3='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_deepbind/keep_exp_RNA.csv'
deepbind_best_cols='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_deepbind/best_col_means.csv'
deepbind_bestscores1='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_deepbind/bestscores_onstep2.csv'
deepbind_bestscores2_1='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_deepbind/bestscores_exp_PBM.csv'
deepbind_bestscores2_2='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_deepbind/bestscores_exp_ChIP.csv'
deepbind_bestscores2_3='/mnt/c/Users/nomaillard/Documents/variant_impact_analysis/xtractor_deepbind/bestscores_exp_RNA.csv'

### run extractions
# keep species and experiments (AND logic)
echo 'start deepbind 1st extraction...'
$xtractor extract \
    --input $deepbind_input \
    --sep '\t' \
    --output $deepbind_output1 \
    --network 'deepbind' \
    --filter-pos-file $positions_to_keep \
    --info-cols-file $info_cols \
    --database $database \
    --filter 'keep' \
    --filter-file $deepbind_first_filter
echo 'deepbind first extraction done.'

# keep hepG2 in experiment details (OR logic)
echo 'start second extraction...'
$xtractor extract \
    --input $deepbind_output1 \
    --output $deepbind_output2 \
    --network 'deepbind' \
    --info-cols-file $info_cols \
    --database $database \
    --filter 'keep' \
    --filter-file $deepbind_second_filter \
    --logic 'OR'
echo 'deepbind second extraction done.'

# filter on experiment PBM
echo 'start extraction on experiment PBM...'
$xtractor extract \
    --input $deepbind_output2 \
    --output $deepbind_output3_1 \
    --network 'deepbind' \
    --info-cols-file $info_cols \
    --database $database \
    --filter 'keep' \
    --filter-file $deepbind_filter_PBM
echo 'deepbind extraction on experiment PBM done.'

# filter on experiment ChIP
echo 'start extraction on experiment ChIP...'
$xtractor extract \
    --input $deepbind_output2 \
    --output $deepbind_output3_2 \
    --network 'deepbind' \
    --info-cols-file $info_cols \
    --database $database \
    --filter 'keep' \
    --filter-file $deepbind_filter_ChIP
echo 'deepbind extraction on experiment ChIP done.'

# filter on experiment RNAcompete
echo 'start extraction on experiment RNAcompete...'
$xtractor extract \
    --input $deepbind_output2 \
    --output $deepbind_output3_3 \
    --network 'deepbind' \
    --info-cols-file $info_cols \
    --database $database \
    --filter 'keep' \
    --filter-file $deepbind_filter_RNA
echo 'deepbind extraction on experiment RNAcompete done.'

### run bestscores
# best col scores
echo 'start calc best scores on cols of step2 dataset...'
$xtractor bestscores \
    --input $deepbind_output2 \
    --output $deepbind_best_cols \
    --best-col-scores
echo 'deepbind best scores on cols of step2 done.'

# means on step2 dataset
echo 'start calc 50 best means on step2 dataset...'
$xtractor bestscores \
    --input $deepbind_output2 \
    --output $deepbind_bestscores1
echo 'deepbind 50 best means on step2 dataset done.'

# means on PBM filtered dataset
echo 'start calc 50 best means on PBM filtered dataset...'
$xtractor bestscores \
    --input $deepbind_output3_1 \
    --output $deepbind_bestscores2_1
echo 'deepbind 50 best means on PBM filtered dataset done.'

# means on ChIPseq filtered dataset
echo 'start calc 50 best means on ChIPseq filtered dataset...'
$xtractor bestscores \
    --input $deepbind_output3_2 \
    --output $deepbind_bestscores2_2
echo 'deepbind 50 best means on ChIPseq filtered dataset done.'

# means on RNAcompete filtered dataset
echo 'start calc 50 best means on RNAcompete filtered dataset...'
$xtractor bestscores \
    --input $deepbind_output3_3 \
    --output $deepbind_bestscores2_3
echo 'deepbind 50 best means on RNAcompete filtered dataset done.'
